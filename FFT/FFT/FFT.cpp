#include "FFT.h"

FFT::FFT(string const& _path)
{
	path = _path ;
	//if load buffer fails
	if(!buffer.loadFromFile(path)) 
		cout<<"Unable to load buffer, something wrong"<<endl ;
	
	//sound play
	sound.setBuffer(buffer) ;
	sound.setLoop(true);
	sound.play();


	//array of vertex for display events
	VA1.setPrimitiveType(LineStrip); //bar at the bottom of window


	// master blue-white strip in center of window
	VA2.setPrimitiveType(Lines); 
	VA3.setPrimitiveType(LineStrip);

	//number of samples per second (please use 44100 or 48000 only)
	sampleRate = buffer.getSampleRate()*buffer.getChannelCount();
	sampleCount = buffer.getSampleCount() ;
	
	//loading buffer
	if(_bufferSize < sampleCount) bufferSize = _bufferSize;
	else bufferSize = sampleCount ;
	
	//frequency bypass
	mark = 0 ;

	//itarate all of samples on buffer -> push to the window
	for(int i(0) ; i < bufferSize ; i++) 
		window.push_back(0.54-0.46*cos(2*PI*i/(float)bufferSize));

	sample.resize(bufferSize) ;
	VA1.resize(bufferSize) ;
}

void FFT::freshWindow()
{

	//marker is use for locate, in where second is playing
	mark = sound.getPlayingOffset().asSeconds()*sampleRate;

	//if where is still any data from audio file
	if(mark+bufferSize < sampleCount)
	{
		for(int i(mark) ; i < bufferSize+mark ; i++)
		{
			sample[i-mark] = Complex(buffer.getSamples()[i]*window[i-mark],0) ;
			// method to display verticies in Vector2f method (same as glVertex2f etc...)
			VA1[i-mark] = Vertex(
				Vector2f(100,750)+Vector2f((i-mark)/(float)bufferSize*700, //wave lenght
					sample[i-mark].real()*0.003), //0.003 is peak amplitude to display value
				Color::Color(100,100,255,100)
			);
		}
	}
}
void FFT::fft(CArray &x)
{
	const int N = x.size();
	if(N <= 1) return;

	// slicing a Array of vectors
	CArray even = x[slice(0,N/2,2)];
	CArray odd = x[slice(1,N/2,2)];

	fft(even);
	fft(odd);

	for(int k = 0 ; k < N/2 ; k++)
	{
		//fft coordinates
		Complex t = polar(1.0,-2 * PI * k / N) * odd[k];
		x[k] = even[k] + t;
		x[k+N/2] = even[k] - t;
	}
}
//update data & process values
void FFT::update()
{
	//fresh updated window
	freshWindow() ;

	//clear strip before new data come
	VA2.clear() ;
	VA3.clear() ;

	bin = CArray(sample.data(),bufferSize) ;
	fft(bin) ;

	//max amplitude for strip height
	float max = 100000000 ;
	

	lines(max); //default line method
	bars(max); //default bar method
}
//bars
void FFT::bars(float const& max)
{
	VA2.setPrimitiveType(Lines);
	Vector2f position(0,600);
	for(float i(3) ; i < min(bufferSize/2.f,20000.f) ; i*=1.01)
	{
		//apend vector (add vector to array)
		Vector2f sampleP(log(i)/log(min(bufferSize/2.f,20000.f)),abs(bin[(int)i])) ;
		VA2.append(Vertex(position+Vector2f(sampleP.x*800,-sampleP.y/max*500),Color::Blue)) ;
		VA2.append(Vertex(position+Vector2f(sampleP.x*800,0),Color::White)) ;
		VA2.append(Vertex(position+Vector2f(sampleP.x*800,0),Color::Color(255,255,255,100))) ;
		VA2.append(Vertex(position+Vector2f(sampleP.x*800,sampleP.y/max*500/2.f),Color::Color(255,255,255,0))) ;
	}
}
//bars lines
void FFT::lines(float const& max)
{
	VA3.setPrimitiveType(LineStrip);
	Vector2f position(0,600);
	Vector2f sampleP;

	//drawing amplitude
	for(float i(std::max((double)0,cWidth.size()-3e5)) ; i < cWidth.size() ; i++)
	{
		//cWidth -> cascade width/height values
		cWidth[i].position -= Vector2f(0,1) ;
		if(cWidth[i].color.a != 0) cWidth[i].color = Color(100,100,255,20);
	}
	sampleP = Vector2f(log(3)/log(min(bufferSize/2.f,20000.f)),abs(bin[(int)3]));
	cWidth.push_back(Vertex(position+Vector2f(sampleP.x*800,-sampleP.y/max*500),Color::Transparent));
	
	//cycle for 
	for(float i(3) ; i < bufferSize/2.f ; i*=1.02)
	{
		sampleP = Vector2f(log(i)/log(min(bufferSize/2.f,20000.f)),abs(bin[(int)i]));
		cWidth.push_back(Vertex(position+Vector2f(sampleP.x*800,-sampleP.y/max*500),Color::Color(255,255,255,20))) ;
	}

	//push back draw
	cWidth.push_back(Vertex(position+Vector2f(sampleP.x*800,-sampleP.y/max*500),Color::Transparent));


	VA3.clear() ;
	for(int i(std::max((double)0,cWidth.size()-3e5)) ; i < cWidth.size() ; i++) VA3.append(cWidth[i]);
}


//draw window
void FFT::draw(RenderWindow &window)
{
	window.draw(VA1);
	window.draw(VA3);
	window.draw(VA2);
}