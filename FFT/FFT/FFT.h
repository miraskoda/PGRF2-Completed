#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <complex>
#include <valarray>
#include <math.h>

const double PI = 3.1415926535897932384626433 ;
const int  _bufferSize = 16384;

using namespace std ;
using namespace sf ;

typedef complex<double> Complex;
typedef valarray<Complex> CArray;

class FFT
{
public:
	void bars(float const& max);
	void lines(float const& max);
	void draw(RenderWindow &window);
	FFT(string const& _path);
	void freshWindow();
	void fft(CArray &x);
	void update() ;
	

private:
		
	int sampleRate ;
	int sampleCount ;
	int bufferSize ;
	int mark ;
	string path;
	SoundBuffer buffer;
	Sound sound;
	vector<Complex> sample;
	vector<float> window;
	CArray bin;
	VertexArray VA1;
	VertexArray VA2;
	VertexArray VA3;
	vector<Vertex> cWidth;
};

