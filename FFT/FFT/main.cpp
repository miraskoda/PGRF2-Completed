#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "FFT.h"


using namespace std ;
using namespace sf ;



int main()
{

	//window width and height parameters
	int w = 900;
	int h = 900;
	


	RenderWindow window(VideoMode(w,h,32),"UHK-FIM PGRF2");

	string path ;
	int bufferSize ;
	//print info about audio file
	cout<<"Audio file se nachazi ve slozce AUDIO"<<endl;
	cout<< "Pro prehrani audia od skupiny Nightwish, zadejte prosim: night.wav" << endl;
	cout<<"Enter the file name: " ;
	cin>>path ;


	FFT fft("Audio/"+path) ;
	Event event ;

	while(window.isOpen())
	{
		while(window.pollEvent(event)) {}
		//update repaint infos
		fft.update() ;
		//clear before repaint
		window.clear() ;
		fft.draw(window) ;

		//repaint display
		window.display();
	}
	return 0;
}
